from random import randint



name = input("Hi! What is your name? ")

num_guesses = int(input("How many guesses does I get? "))

for guess_num in range(num_guesses):
    m = randint(1,12)

    yyyy = randint(1924,2004)

    print("Guess", guess_num + 1,":", name, "were you born in", m, "/", yyyy,"?")

    answer = input("yes or no? ")

    if answer == "yes" or answer == "Yes" or answer == "YES" :
        print("I knew it!")
    elif answer == "no" or answer == "No" or answer == "NO" :
        if guess_num + 1 == num_guesses :
            print("I have other things to do. Good bye")
        else :
            print("Drat! Lemme try again!")
    else :
        print("Bad input. Please try again.")
